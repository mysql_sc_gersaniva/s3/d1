-- Inserting Records
INSERT INTO artists (name) VALUES("Rivermaya");
INSERT INTO artists (name) VALUES("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Psy 6",
	"2012-1-1",
	2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Trip",
	"1996-1-1",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Gangnam Style",
	253,
	"K-Pop",
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kundiman",
	234,
	"OPM",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Kisapmata",
	259,
	"OPM",
	1
);

-- Selecting Records
-- Display ALL columns for all songs
SELECT * FROM songs;

-- Display the song_name and genre of all songs
SELECT song_name, genre FROM songs;

-- Display the song_name of all OPM songs
SELECT song_name FROM songs WHERE genre = "OPM";


-- Display the song name and length of OPM songs That are longer than 2 minutes and 40 seconds

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

-- We can use AND and OR for multiple expressions in the WHERE clause


-- Updating records
-- Updating the length of Kundiman to 2:40
UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

--Removing the WHERE clause will update ALL rows



-- Deleting records
-- Deleting all OPM songs longer than 2:40
DELETE FROM songs WHERE genre = "OPM"  AND length > 240;

--Removing the WHERE clause will delete ALL rows
